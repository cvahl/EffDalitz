# The macros

## makePlots_2DEff
This macro computes efficiency over 2D distribution, optionally reweighting some 2D input distributions.

## makeBDTEff, makePIDEff
These scripts compute the BDT and PID efficiency distributions.

## combPlots
The combPlots macro combines 1D or 2D input histograms, using the basic mathematical operations of sum, difference, multiplication and division.
This is useful if you need to combine efficiency distributions.

## meanEff
Script to compute the mean efficiency over a given distribution.

# Option files

## makePlots_2DEff
[Here](config/plot2DEff_MC.info) you can find a simple configuration example for the makePlots_2DEff macro, where:
- some general options are specified;
- some options are set for the efficiency computation.

Some details about the general options of the example:
- DecayTree: name of the input TTree from which read the variables to make the Dalitz plot;
- cuts: string to specify which cuts you want to use, to select the candidates for the Dalitz plot;
- var1_name, var2_name: names of the two variables that you want to use for the 2D efficiency distribution;
- var_numbins, var_low, var_high: number of bins, lower and higher boundaries for both var1 and var2;
- out_name: output name of the plots, saved in the output root file;
- x_axis_label, y_axis_label: x and y labels of the final plots;
- log_file: path and name of the output log file.

The options related to the efficiency section are the same of the makePlots_Dalitz macro, described above.

## makeBDTEff
[Here](config/BDTEff.info) there is an example of configuration file for that macro.

You have to specify:
- bdtcut: BDT cut value;
- inHisto_name: name of the input histogram containing the distribution of particles depending on the BDT response;
- outHisto_name: name of te output histo containing the efficiency distribution;
- adaptive: to use the adaptive binning (value = 1) or the TEfficiency (value = 0).

## mean_eff
[Here](config/meanEff.info) you find a configuration example for the meanEff script. You have to specify:
- adaptive: if you are using adaptive binning or not;
- inHistoEff_name: the name of the efficiency histogram; 
- inTreeEvents_name: the name of the tree containing the event distribution over wich you want to compute the mean efficiency;
- conditions: some conditions that you would use to select the event distribution;
- var_1_name, var_2_name: the names of the variables on which the efficiency is map;
- outLog_name: the name of the output log file containing the results.

## effLUT
[Here](config/effLUT.info) you can find an option file to configure the efficiency correction for the signal yield computation.
There, you have to specify the options for the kinematic, BDT and PID efficiencies.

You first have to set to use the adaptive binning or the TEfficiency objects, then the names of the E and momentum variables as they are in the dataset that you want to correct.

About the kinematic correction:
- KinEff: a boolean to switch on/off the kinematic correction;
- inFile, inHistoEff: name of the file containing the 2D kinematic efficiency distribution; the 2D histo name has to be specified too; 
- Kin_1_name, Kin_2_name: names of the kinematic variables as they are specified in the dataset, used to retrieve the correct phase space point in the 2D efficiency distribution.

Actually, the effLUT macro is able to handle with two different BDT cuts, on the same or on different particles.
Some specification, for the first BDT cut only:
- BDTEff_1: a boolean to switch on/off the first BDT cut correction;
- inFile_1, inHistoEff_1: name of the file containing the BDT efficiency distribution; the histo name has to be specified too;
- var1, var2: names of the kinematic variables used to retrieve the BDT efficiency.

Finally, some words about the PID efficiency:
- PIDEff: a boolean to switch on/off the PID efficiency correction;
- inFile, inHistoEff: name of the file containing the PID efficiency distribution; the histo name has to be specified too; 
- part_name_: name of the particle on which you are applying the PID cut;
- ntracks_name: name of the variable corresponding to the number of tracks in the event, an input quantity for the PID efficiency.
