/*!
 *  @file      makePlots_MCEff.cpp
 *  @author    Alessio Piucci
 *  @brief     Script to make plots of selection efficiency on MC.
 *  @return    Returns the a .root file containing the plots.
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string>
#include <unistd.h>  //to use getopt in the parser

//ROOT libraries
#include <TString.h>
#include <TFile.h>
#include <TTree.h>
#include <TH1D.h>

//Boost libraries
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string/replace.hpp>

using namespace std;
namespace pt = boost::property_tree;


int main(int argc, char** argv){
  
  //-----------------------//
  //  parsing job options  //
  //-----------------------//
  
  std::string configFileName = "";
  TString inFileName = "";
  TString outFileName = "";
  
  extern char* optarg;

  int ca;
  
  //parsing the input options
  while ((ca = getopt(argc, argv, "c:i:o:h")) != -1){
    switch (ca){

    case 'c':
      configFileName = optarg;
      break;
      
    case 'o':
      outFileName = optarg;
      break;
    
    case 'i':
      inFileName = optarg;
      break;

    case 'h':
      std::cout << "-- Help --" << std::endl;
      std::cout << "-c : configuration file name." << std::endl;
      std::cout << "-i : input file name." << std::endl;
      std::cout << "-o : output file name." << std::endl;
      std::cout << std::endl;
      exit(EXIT_FAILURE);
      break;

    default :
      std::cout << "Error: not recognized option. Type -h for the help." << std::endl;
      exit(EXIT_FAILURE);
      
    }  //switch (ca)
  }  //while ((ca = getopt(argc, argv, "i:o")) != -1)
  
  std::cout << "configFileName = " << configFileName << std::endl;
  std::cout << "inFileName = " << inFileName << std::endl;
  std::cout << "outFileName = " << outFileName << std::endl;
  
  //check of the acquired options
  if((inFileName == "") || (outFileName == "") || (configFileName == "")){
    std::cout << "Error: input or output file names not correctly set." << std::endl;
    exit(EXIT_FAILURE); 
  }
  
  
  //----------------------------------//
  //  reading the configuration file  //
  //----------------------------------//
  
  //create empty property tree object
  pt::ptree configtree;
  
  //parse the INFO into the property tree.
  pt::read_info(configFileName, configtree);
  
  
  //----------------------//
  //  reading input data  //
  //----------------------//
  
  //reading input file
  TFile* inFile = TFile::Open(inFileName,"READ");
  
  if(inFile == nullptr){
    std::cout << "Error: input file does not exist." << std::endl;
    exit(EXIT_FAILURE);  
  }
  
  //reading input tree containing the MC informations
  TTree* inTree_gen = (TTree*) inFile->Get("MCDecayTree_fixed_2");
  
  if(inTree_gen == nullptr){
    std::cout << "Error: input tree of MC particles does not exist." << std::endl;
    exit(EXIT_FAILURE);
  }

  //reading input tree containing the reconstructed particles
  TTree* inTree_reco = (TTree*) inFile->Get("DecayTree");
  
  if(inTree_gen == nullptr){
    std::cout << "Error: input tree of reconstructed particles does not exist." << std::endl;
    exit(EXIT_FAILURE); 
  }
  
  
  //----------------//
  //  making plots  //
  //----------------//
  
  //opening output file
  TFile* outFile = new TFile(outFileName, "RECREATE");
  
  //variables used to define the histograms
  std::string particle_name;
  std::string plot_name;
  std::string formula_name;
  std::string formula_tree;
  std::string condition_tree;
  
  unsigned int num_bins;
  double xaxis_low;
  double xaxis_high;
  
  //loop over particles
  BOOST_FOREACH(pt::ptree::value_type &particle, configtree.get_child("particles")){
    
    //loop over quantities to plot
    BOOST_FOREACH(pt::ptree::value_type &quantity, configtree.get_child("quantities")){
      
      //acquiring data from the configuration file
      particle_name = particle.second.get<std::string>("name");
      
      formula_name = quantity.second.get<std::string>("formula");
      
      //formatting the formula from the configuratio file
      boost::replace_all(formula_name, "<par>", particle_name);
        
      plot_name = quantity.second.get<std::string>("name");
      num_bins = quantity.second.get<unsigned int>("num_bins");
      xaxis_low = quantity.second.get<double>("low");
      xaxis_high = quantity.second.get<double>("high");
      
      //filling the histogram with generated quantities
      TH1D* h_temp_gen = new TH1D("h_temp_gen",
                                  plot_name.c_str(),   //axis title
                                  num_bins,             //number of bins
                                  xaxis_low,
                                  xaxis_high);          //plot range
      
      //to store the propagated errors
      h_temp_gen->Sumw2();

      //formula to select the entries and to fill the histogram
      formula_tree = "(" + formula_name + ")>>h_temp_gen";
      
      //filling the generated histogram
      inTree_gen->Draw(formula_tree.c_str(), "", "");
      
      //filling the histogram with reconstructed quantities
      TH1D* h_temp_reco = new TH1D("h_temp_reco",
                                   plot_name.c_str(),   //axis title
                                   num_bins,             //number of bins
                                   xaxis_low,
                                   xaxis_high);          //plot range
      
      //to store the propagated errors
      h_temp_reco->Sumw2();
      
      //formula to select the entries and to fill the histogram
      formula_tree = "(" + formula_name + ")>>h_temp_reco";
      
      //condition to select reconstructed candidates
      condition_tree = "("+configtree.get<std::string>("options.low_BKGCat")+"<="+particle_name+"_BKGCAT)&&("+particle_name+"_BKGCAT<="+configtree.get<std::string>("options.high_BKGCat")+")";
      
      //filling the reconstructed histogram
      inTree_reco->Draw(formula_tree.c_str(), condition_tree.c_str(), "");
      
      //defining the final histogram
      TH1D* h_temp_eff = new TH1D("h_temp_eff",
                                  plot_name.c_str(),   //axis title
                                  num_bins,             //number of bins
                                  xaxis_low,
                                  xaxis_high);          //plot range 
      
      //to store the propagated errors
      h_temp_eff->Sumw2();
      
      //computing the efficiency
      h_temp_eff->Divide(h_temp_reco, h_temp_gen, 1., 1., "B");
      
      //a little bit of style
      h_temp_gen->GetXaxis()->SetTitle((quantity.second.get<std::string>("x_axisName")).c_str());
      h_temp_gen->GetYaxis()->SetTitle("entries");
      h_temp_gen->GetXaxis()->SetTitleOffset(1.3);
      h_temp_gen->GetYaxis()->SetTitleOffset(1.4);
      h_temp_gen->SetLineColor(kBlack);
      
      h_temp_eff->GetXaxis()->SetTitle((quantity.second.get<std::string>("x_axisName")).c_str());
      h_temp_eff->GetYaxis()->SetTitle((quantity.second.get<std::string>("y_axisName")).c_str());
      h_temp_eff->GetXaxis()->SetTitleOffset(1.3);
      h_temp_eff->GetYaxis()->SetTitleOffset(1.4);
      h_temp_eff->SetLineColor(kBlue);
      
      
      //save in the output root file
      outFile->cd();
      
      h_temp_gen->Write(("h_" + particle_name + "_" + plot_name + "_gen").c_str());
      h_temp_eff->Write(("h_" + particle_name + "_" + plot_name).c_str());
      
      //cleaning up memory!
      delete h_temp_gen;
      delete h_temp_reco;
      delete h_temp_eff;
      
    }  //loop over quantities to plot
  }  //loop over particles
  
  //writing and closing output file
  outFile->Write();
  outFile->Close();
  
  return 0;
}
