/*!                                  
 *  @file      makePlots_2DEff.cpp                   
 *  @author    Alessio Piucci                                    
 *  @brief     A macro to make 2D efficiencies.                         
 */

#include "../include/Eff.h"
#include "../include/commonLib.h"
#include <../IOjuggler/IOjuggler.h>

// Include files
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string>
#include <unistd.h>  //to use getopt in the parser

//ROOT libraries
#include <TROOT.h>
#include <TFile.h>
#include <TTree.h>
#include <TH2D.h>
#include <TString.h>
#include <TFriendElement.h>
#include <TObjString.h>

//Boost libraries
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string/replace.hpp>
#include <boost/algorithm/string.hpp>

using namespace std;
namespace pt = boost::property_tree;


int main(int argc, char** argv){
  
  //---------------------//
  //  parse job options  //
  //---------------------//
  
  std::string inFileName_num = "";
  std::string inFileName_den = "";
  std::string outFileName = "";
  std::string configFileName = "";
  
  extern char* optarg;
  extern int optind;
  
  int ca;
  
  //parse the input options
  while ((ca = getopt(argc, argv, "i:o:c:d::h")) != -1){
    switch (ca){

    case 'i':
      inFileName_num = optarg;
      break;

    case 'd':
      inFileName_den = optarg;
      break;
      
    case 'o':
      outFileName = optarg;
      break;
      
    case 'c':
      configFileName = optarg;
      break;
      
    case 'h':
      std::cout << "-- Help --" << std::endl;
      std::cout << "-i : input file name." << std::endl;
      std::cout << "-d : OPTIONAL: input file name with all events (denominator)."
                << " If not specified, is taken equal to the -i argument" << std::endl;
      std::cout << "-o : output file name." << std::endl;
      std::cout << "-c : configuration file name." << std::endl;
      std::cout << std::endl;
      exit(EXIT_FAILURE);
      break;
    
    default :
      std::cout << "Error: not recognized option. Type -h for the help." << std::endl;
      exit(EXIT_FAILURE);
    
    }  //switch (ca)
  }  //while ((ca = getopt(argc, argv, "i:o:c:h")) != -1)
  
  std::vector<TString> extraopts;
  
  while (optind < argc)
    //use emplace_back to call TString ctor
    extraopts.emplace_back(argv[optind++]);
  
  //for the denominator: if it was not parsed from command line,
  //use the same TFile of the denominator
  if(inFileName_den == "")
    inFileName_den = inFileName_num;
  
  //print the parsed options
  std::cout << "inFileName_num = " << inFileName_num << std::endl;
  std::cout << "inFileName_den = " << inFileName_den << std::endl;
  std::cout << "outFileName = " << outFileName << std::endl;
  std::cout << "configFileName = " << configFileName << std::endl;
  
  if(!extraopts.empty())
    for(const auto& opt : extraopts)
      std::cout << "friendfile:friendtree = " << opt.Data() << std::endl;
  
  //check of the parsed options
  if((configFileName == "") || (inFileName_num == "") || (outFileName == "")){
    std::cout << "Error: input configuration not correctly set." << std::endl;
    exit(EXIT_FAILURE);
  }
  
  std::cout << "meep2" << std::endl;
  
  //configuration Boost tree
  pt::ptree configtree;
  
  //parse the INFO into the property tree
  pt::read_info(configFileName, configtree);
  auto_append_in_ptree(configtree);
  
  std::cout << "meep3" << std::endl;
  
  //open the input file
  TFile* inFile = TFile::Open(ParseEnvName(inFileName_num).c_str(), "READ");
  
  if(inFile == nullptr){
    std::cout << "Error: input file does not exist." << std::endl;
    exit(EXIT_FAILURE);
  }
  
  std::cout << "meep5" << std::endl;
  
  //open the input tree
  TTree* inTree = (TTree*) inFile->Get((configtree.get<std::string>("options.DecayTree")).c_str());
  
  if(inTree == nullptr){
    std::cout << "Error: input tree does not exist." << std::endl;
    exit(EXIT_FAILURE);
  }

  std::cout << "meep6" << std::endl;
  
  std::cout << "inFileName = " << inFileName_num
            << ", inTree = " << configtree.get<std::string>("options.DecayTree") << std::endl;
  
  //add friend tree(s). for this we have to fiddle apart file and friendtree in the nonoptions, i.e. <file:friendtree>
  //and because they will go out of scope, we have to push them into a vector
  std::vector< TFriendElement* > fes;
  
  std::cout << "meep4" << std::endl;
  
  if(!extraopts.empty())
    for(const auto& opt : extraopts){
      auto ff = TFile::Open(ParseEnvName(static_cast<std::string>(static_cast<TObjString*>((opt.Tokenize(":")->At(0)))->String().Data())).c_str());
      fes.push_back(inTree->AddFriend(static_cast<TObjString*>((opt.Tokenize(":")->At(1)))->String().Data(),ff));
      
    }
  
  ////////
  ////////

  std::cout << "meep" << std::endl;
  
  //create an Eff object
  Eff* EffPlot = new Eff(configtree, inFileName_num, outFileName);
  
  TH2D* h_2D_reco = new TH2D();
  TH2D* h_2D_gen = new TH2D();
  
  TH2D* h_2D_errors_gen = new TH2D();
  TH2D* h_2D_errors_reco = new TH2D();
  
  //variables used for the normalization of histograms
  unsigned int num_gen_events = 0;
  unsigned int num_reco_events = 0;
  
  //compute the 2D plot for reconstructed events
  h_2D_reco = EffPlot->Make2DPlot(inTree, false, num_reco_events);
  
  //efficiency computation?
  if((bool) configtree.get<unsigned int>("efficiency.EffOverMC")){
    
    //open the input file with the MC variables
    TFile* inFile_MC = TFile::Open(inFileName_den.c_str(),"READ");
    
    if(inFile_MC == nullptr){
      std::cout << "Error: input file with MC variables for the efficiency  does not exist." << std::endl;
      exit(EXIT_FAILURE);
    }
    
    //open the input tree with the MC variables
    TTree* inTree_MC = (TTree*) inFile_MC->Get((configtree.get<std::string>("efficiency.MCDecayTree")).c_str());

    if(inTree_MC == nullptr){
      std::cout << "Error: input tree MC does not exist." << std::endl;
      exit(EXIT_FAILURE);  
    }
    
    //compute the Dalitz plot for generated events
    h_2D_gen = EffPlot->Make2DPlot(inTree_MC, true, num_gen_events);

    //retrieve the number of generated and reconstructed events from the samples,
    //and set them in the Eff library for the rescaling
    EffPlot->SetGenRecoEntries(inTree_MC->GetEntries(), inTree->GetEntries());
    
    EffPlot->Compute2DEfficiency(h_2D_gen, h_2D_reco,
                                 h_2D_errors_gen, h_2D_errors_reco);  
  }
  
  //write the output files
  EffPlot->Finalize();
  
  return 0;
}
