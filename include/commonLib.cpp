/*!
 *  @file      commonLib.cpp
 *  @author    Alessio Piucci
 *  @brief     Common library of methods used by different macros and structures.
 */

#include "commonLib.h"

using namespace std;

//-----------------------------------
// Parse a string containing some environment variables
//-----------------------------------
std::string ParseEnvName(std::string input_string){
  
  //I first search the env variable in the input string
  size_t start_pos = input_string.find("$");
  
  if(start_pos == std::string::npos)
    return input_string;  //if no $ are find, there are no env variables to convert
  else
  {
    std::string env_variable = input_string.substr(start_pos, input_string.find("/", start_pos + 1));
    
    //to convert the env variable, I have to skip the first $ char
    char *env_variable_converted = getenv((env_variable.substr(1, env_variable.size())).c_str());

    std::cout << "Converted " << env_variable << " --> " << env_variable_converted << std::endl;
    
    input_string.replace(start_pos, input_string.find("/", start_pos + 1), env_variable_converted);

    //now search for the next occurance of an env variable
    return ParseEnvName(input_string);
  }
}

//-----------------------------------
// Retrieve and parse a set of cuts
//-----------------------------------
std::string ParseCuts(pt::ptree configtree, bool denominator_eff){
  
  std::string cuts_string = "";
  
  //retrieve the cuts
  if(!denominator_eff){
    cuts_string = configtree.get<std::string>("options.cuts");

    //If I have to compute the efficiency, I also append the cuts used for the denominator
    if((bool) configtree.get<unsigned int>("efficiency.EffOverMC")){
      
      if((cuts_string != "") && (configtree.get<std::string>("efficiency.cuts") != ""))
        cuts_string += " && ";
      
      cuts_string += configtree.get<std::string>("efficiency.cuts");
    }
  }
  else
  {
    cuts_string = configtree.get<std::string>("efficiency.cuts");
    
    //check if I have to apply cuts to the denominator only,
    //for some extremely specific reason
    if(configtree.get_optional<std::string>("efficiency.cutsNOTORECO")){
      
      //check if I have a not-empty second kinematic cut
      if(configtree.get<std::string>("efficiency.cutsNOTORECO") != ""){
        
        if(cuts_string != "")
          cuts_string += " && ";
        
        cuts_string += configtree.get<std::string>("efficiency.cutsNOTORECO");
      }
    }  //if(configtree.get_optional<std::string>("efficiency.cutsNOTORECO"))
  }  //if(!denominator_eff)

  return cuts_string;
  
}

//-----------------------------------
// Compute the binomial error, mainly for efficiency computation
//-----------------------------------
double BinomialError(int passed, int tries){
  return sqrt(passed * (tries - passed) / (double) pow(tries, 3.));
}

//-----------------------------------
// Compute the error of a generic ratio
//-----------------------------------
double ErrorRatio(double num, double num_err, double den, double den_err){
  return sqrt( pow(num_err/den, 2.)
               + pow(num*den_err/(pow(den, 2.)), 2.));
}

//----------------------------------- 
//  Fix weights problems for a TEfficiency, TH2D
//----------------------------------- 
void FixTEfficiencyWeights(TH2D* h_pass, TH2D* h_total){
  
  double stat_pass[TH1::kNstat];
  double stat_total[TH1::kNstat];
  
  h_pass->GetStats(stat_pass);
  h_total->GetStats(stat_total);
  
  //require: sum of weights == sum of weights^2
  if((fabs(stat_pass[0] - stat_pass[1]) > 1e-5) ||
     (fabs(stat_total[0] - stat_total[1]) > 1e-5)) 
    std::cout << "stat_total[0] = " << stat_total[0] 
              << ", stat_total[1] = " << stat_total[1]
              << ", stat_pass[0] = " << stat_pass[0]
              << ", stat_pass[1] = " << stat_pass[1]
              << std::endl;
  
  std::cout << std::endl;
  
  return;
}

//-----------------------------------
//  Check the consistency of generated and reco histos for TH2A
//-----------------------------------
void CheckConsistency_adaptive(TH2A *h_gen, TH2A *h_reco, unsigned int &num_fixed_bins, bool fix = false){
  
  if(! h_gen->HasSameBinning(*h_reco)){
    std::cout << "Error: generated and reconstructed TH2A histos have different binning." << std::endl;
    exit(EXIT_FAILURE);
  }
  
  double num_total;
  double num_passed;
  
  //loop over bins
  for(unsigned int i_bin = 1; i_bin <= h_gen->GetNBins(); ++i_bin){
    
    num_total = h_gen->GetBinContent(i_bin);
    num_passed = h_reco->GetBinContent(i_bin);
    
    if(num_passed > num_total){
      std::cout << "Warning: num_passed > num_total. num_passed = " << num_passed
                << ", num_total = " << num_total;
      
      if(fix){
        std::cout << ". Fixing num_passed to num_total.";
        h_reco->SetBinContent(i_bin, h_gen->GetBinContent(i_bin));
      }
      
      std::cout << std::endl;
      
      ++num_fixed_bins;
      
    }
  }  //loop over bins
  
  return;
}
