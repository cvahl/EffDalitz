/*!
 *  @file      effLUT.cpp
 *  @author    Alessio Piucci
 *  @brief     Script to retrieve the efficiency value for a given candidate, reading from lookup tables.
 *  @return    Returns the efficiency value.                   
 */

#include "effLUT.h"

//----------------------------------------------
// Get efficiency as function of full kinematics of the event
//----------------------------------------------
void effLUT::GetEventEff(const double Kin_1, const double Kin_2,
                         const double BDTpt_1, const double BDTfd_1,
                         const double BDTpt_2, const double BDTfd_2,
                         const std::vector<TLorentzVector> PIDqvects,
                         const unsigned int nTracks, const bool varxP_varyEta,
                         Efficiency &KinEff_tot,
                         Efficiency &BDTEff_tot,
                         Efficiency &PIDEff_tot,
                         Efficiency &comb_eff, const int& year){
  
  //----------------------------//
  //  compute the efficiencies  //
  //----------------------------//
  
  //kinematic efficiency
  GetKineEff(Kin_1, Kin_2, KinEff_tot);
  
  //BDT efficiency
  GetBDTEff(BDTpt_1, BDTfd_1, BDTpt_2, BDTfd_2, BDTEff_tot, year);
  
  //PID efficiency
  GetPIDEff(PIDqvects, nTracks, varxP_varyEta, PIDEff_tot);
  
  //check the signal efficiencies one more time
  if((!CheckValidEfficiency(KinEff_tot))
     || (!CheckValidEfficiency(BDTEff_tot))
     || (!CheckValidEfficiency(PIDEff_tot))){
    
    /*
    std::cout << "Warning: one of the single efficiencies is not fine."
              << " Setting the combined to unvalid state."
              << " KinEff_tot = " << KinEff_tot.efficiency << " +- " << KinEff_tot.eff_err
              << ", BDTEff_tot = " << BDTEff_tot.efficiency << " +- " << BDTEff_tot.eff_err
              << ", PIDEff_tot = " << PIDEff_tot.efficiency << " +- " << PIDEff_tot.eff_err
              << std::endl;
    */
    
    SetUnvalidEfficiency(comb_eff);
    
    return;
  }
  
  //----------------------------------------------------//
  //  finally, I can compute the combined efficiencies  //
  //----------------------------------------------------//
  
  CombineEfficiencies(comb_eff, std::vector<Efficiency>{KinEff_tot, BDTEff_tot, PIDEff_tot});
  
  //check for a fatal error, that should never happen
  if(!CheckValidEfficiency(comb_eff)){
    std::cout << "Error: what the hell: the single efficiencies were fine, but the combined one is unvalid."
              << " KinEff_tot = " << KinEff_tot.efficiency << " +- " << KinEff_tot.eff_err
              << ", BDTEff_tot = " << BDTEff_tot.efficiency << " +- " << BDTEff_tot.eff_err
              << ", PIDEff_tot = " << PIDEff_tot.efficiency << " +- " << PIDEff_tot.eff_err << std::endl;
    exit(EXIT_FAILURE);
  }

  return;
  
}

//---------------------------------------------------------
// Combine single efficiencies
//---------------------------------------------------------
void effLUT::CombineEfficiencies(Efficiency &TotEff, const std::vector<Efficiency> Eff_vect){
  
  //prepare the overall efficiency for the combination of the single efficiencies
  SetEfficiencyOne(TotEff);
  
  //now combine all PID cuts to get the combined efficiency and the number of total and passed events
  for(std::vector<Efficiency>::const_iterator it_vect = Eff_vect.begin();
      it_vect != Eff_vect.end(); ++it_vect){
    
    TotEff.efficiency *= (*it_vect).efficiency;
    
    TotEff.num_total += (*it_vect).num_total;
    TotEff.num_passed += (*it_vect).num_passed;
  }
  
  //finally combine the single errors, to get the final one
  //given the total efficiency E_tot = E_1 * E_2 * ... * E_n, the final error is:
  //sqrt( sum_i ( (E_tot/E_i) * dE_i)^2 )
  for(std::vector<Efficiency>::const_iterator it_vect = Eff_vect.begin();
      it_vect != Eff_vect.end(); ++it_vect){
    TotEff.eff_err += pow((*it_vect).eff_err * (TotEff.efficiency / (*it_vect).efficiency), 2.);
    TotEff.eff_err_low += pow((*it_vect).eff_err_low * (TotEff.efficiency / (*it_vect).efficiency), 2.);
    TotEff.eff_err_up += pow((*it_vect).eff_err_up * (TotEff.efficiency / (*it_vect).efficiency), 2.);
  }
  
  TotEff.eff_err = sqrt(TotEff.eff_err);
  TotEff.eff_err_low = sqrt(TotEff.eff_err_low);
  TotEff.eff_err_up = sqrt(TotEff.eff_err_up);
  
  return;
  
}

//---------------------------------------------------------
// Get a single efficiency from a 2D TEfficiency object, with number of total and passed events
//---------------------------------------------------------
void effLUT::GetSingleEff(const double var_1, const double var_2,
                          TEfficiency *h_eff, Efficiency &Eff){
  
  //find bin for these coordinates, but first check if the ranges are ok
  if( (var_1 < h_eff->GetTotalHistogram()->GetXaxis()->GetXmin())
      || (h_eff->GetTotalHistogram()->GetXaxis()->GetXmax() < var_1)
      || (var_2 < h_eff->GetTotalHistogram()->GetYaxis()->GetXmin())
      || (h_eff->GetTotalHistogram()->GetYaxis()->GetXmax() < var_2)
     ){

    log_errors_stream << "Warning: values out of histo " << h_eff->GetName() 
                      << ", var_1 = " << var_1 << ", var_2 = " << var_2
                      << ". Setting dummy efficiency to -1, in order to be skipped." << std::endl;
    
    SetUnvalidEfficiency(Eff);
    
    return;
  }
  
  // Find bin for these coordinates
  int i_bin = h_eff->FindFixBin(var_1, var_2);
  
  //get total and passed events
  double num_total = h_eff->GetTotalHistogram()->GetBinContent(i_bin);
  double num_passed = h_eff->GetPassedHistogram()->GetBinContent(i_bin);
  
  //if there are no generated events, set the efficiency to 0.
  if(num_total == 0){
    SetNullEfficiency(Eff);
    return ;
  }
  
  //efficiency and errors error, from the upper and lower confidence levels
  double eff = num_passed / num_total;
  double low_CL = TEfficiency::Wilson(num_total, num_passed, 0.6832, false);
  double up_CL = TEfficiency::Wilson(num_total, num_passed, 0.6832, true);
  
  Eff.efficiency = eff;
  Eff.eff_err = (up_CL - low_CL)/2.;
  Eff.eff_err_low = eff - low_CL;
  Eff.eff_err_up = up_CL - eff;
  
  //total and passed events
  Eff.num_total = num_total;
  Eff.num_passed = num_passed;
  
  return;
}

//---------------------------------------------------------
// Get a single efficiency from a TH2A object, with number of total and passed events
//---------------------------------------------------------
void effLUT::GetSingleEff_adaptive(const double var_1, const double var_2,
                                   TH2A *h_eff, Efficiency &Eff){
  
  //find bin for these coordinates, but first check if the ranges are ok
  if( (var_1 < h_eff->GetMinX()) || (h_eff->GetMaxX() < var_1)
      || (var_2 < h_eff->GetMinY()) || (h_eff->GetMaxY() < var_2)
     ){
    
    log_errors_stream << "Warning: values out of histo " << h_eff->GetName()
                      << ", var_1 = " << var_1
                      << " [" << h_eff->GetMinX() << ", " << h_eff->GetMaxX() << "]" 
                      << ", var_2 = " << var_2
                      << " [" << h_eff->GetMinY() << ", " << h_eff->GetMaxY() << "]"
                      << ". Setting dummy efficiency to -1, in order to be skipped." << std::endl;
    
    SetUnvalidEfficiency(Eff);
    
    return;
  }
  
  // Find bin for these coordinates
  int i_bin = h_eff->FindBin(var_1, var_2);
  
  //efficiency
  Eff.efficiency = h_eff->GetBinContent(i_bin);
  
  //efficiency error
  Eff.eff_err = h_eff->GetBinError(i_bin);
  Eff.eff_err_low = h_eff->GetBinErrorLow(i_bin);
  Eff.eff_err_up = h_eff->GetBinErrorUp(i_bin);
  
  //for the total and passed events, just use a reference value
  Eff.num_total = 100.;
  Eff.num_passed = 100.*Eff.efficiency;
  
  return;
}

//---------------------------------------------------------
// Get a single efficiency from a TH2A object, with number of total and passed events
//---------------------------------------------------------
void effLUT::GetSingleEff_adaptive_yr(const double& var_1, const double& var_2, const int& year,
                                      std::vector<std::pair<TH2A*,int>>& hs_eff, Efficiency &Eff){

  for(const auto& h_eff : hs_eff){
    if(h_eff.second != year) continue;

    //find bin for these coordinates, but first check if the ranges are ok
    if( (var_1 < h_eff.first->GetMinX()) || (h_eff.first->GetMaxX() < var_1)
        || (var_2 < h_eff.first->GetMinY()) || (h_eff.first->GetMaxY() < var_2)
       ){

      log_errors_stream << "Warning: values out of histo " << h_eff.first->GetName()
                        << ", var_1 = " << var_1
                        << " [" << h_eff.first->GetMinX() << ", " << h_eff.first->GetMaxX() << "]"
                        << ", var_2 = " << var_2
                        << " [" << h_eff.first->GetMinY() << ", " << h_eff.first->GetMaxY() << "]"
                        << ". Setting dummy efficiency to -1, in order to be skipped." << std::endl;

      SetUnvalidEfficiency(Eff);

      return;
    }
    // Find bin for these coordinates
    int i_bin = h_eff.first->FindBin(var_1, var_2);

    //efficiency
    Eff.efficiency = h_eff.first->GetBinContent(i_bin);

    //efficiency error
    Eff.eff_err = std::max(std::fabs(h_eff.first->GetBinErrorLow(i_bin)),std::fabs(h_eff.first->GetBinErrorUp(i_bin)));
    Eff.eff_err_low = std::fabs(h_eff.first->GetBinErrorLow(i_bin));
    Eff.eff_err_up = std::fabs(h_eff.first->GetBinErrorUp(i_bin));

    //for the total and passed events, just use a reference value
    Eff.num_total = 100.;
    Eff.num_passed = 100.*Eff.efficiency;

    return;
  }
  log_errors_stream << "Something wrong with parsing years. Take a look into the source code!" << std::endl;
}

//---------------------------------------------------------
// Get efficiency of kinematic selection
// from two different 2D efficiency distributions
//---------------------------------------------------------
void effLUT::GetKineEff(const double Kin_1, const double Kin_2, Efficiency &KinEff_tot){
  
  Efficiency GeneratorEff;
  Efficiency TriggerToSelEff;
  
  //-------------------------------------//
  //  retrieve the generator efficiency  //
  //-------------------------------------//
  
  //is the generator efficiency switched on?
  if(Generator_eff_on){
    
    //retrieve the efficiency from the generator efficiency look up table
    if(adaptive_eff)
      GetSingleEff_adaptive(Kin_1, Kin_2, h_generator_eff_adaptive, GeneratorEff);
    else
      GetSingleEff(Kin_1, Kin_2, h_generator_eff, GeneratorEff);
  }
  else
    SetEfficiencyOne(GeneratorEff);
  
  //check the generator efficiency
  if(!CheckValidEfficiency(GeneratorEff)){
    
    log_errors_stream << "Warning: Kin_1 = " << Kin_1  << ", Kin_2 = " << Kin_2
                      << ", GeneratorEff.eff = " << GeneratorEff.efficiency
                      << " +- " << GeneratorEff.eff_err
                      << ", GeneratorEff.num_total = " << GeneratorEff.num_total
                      << ", GeneratorEff.num_passed = " << GeneratorEff.num_passed
                      << ". Setting unvalid efficiency." << std::endl;
    
    SetUnvalidEfficiency(GeneratorEff);
    SetUnvalidEfficiency(KinEff_tot);
    
    return;
  }
  
  //----------------------------------------------//
  //  retrieve the trigger->selection efficiency  //
  //----------------------------------------------//
  
  //is the trigger->selection efficiency switched on?
  if(TriggerToSel_eff_on){
    if(adaptive_eff)
      GetSingleEff_adaptive(Kin_1, Kin_2, h_TriggerToSel_eff_adaptive, TriggerToSelEff);
    else
      GetSingleEff(Kin_1, Kin_2, h_TriggerToSel_eff, TriggerToSelEff);
  }
  else
    SetEfficiencyOne(TriggerToSelEff);
  
  //check the trigger->selection efficiency
  if(!CheckValidEfficiency(TriggerToSelEff)){
    
    log_errors_stream << "Warning: Kin_1 = " << Kin_1  << ", Kin_2 = " << Kin_2
                      << ", TriggerToSelEff.eff = " << TriggerToSelEff.efficiency
                      << " +- " << TriggerToSelEff.eff_err
                      << ", TriggerToSelEff.num_total = " << TriggerToSelEff.num_total
                      << ", TriggerToSelEff.num_passed = " << TriggerToSelEff.num_passed
                      << ". Setting unvalid efficiency." << std::endl;
    
    SetUnvalidEfficiency(TriggerToSelEff);
    SetUnvalidEfficiency(KinEff_tot);
    
    return;
  }
  
  
  //--------------------------------------//
  //  combine the kinematic efficiencies  //
  //--------------------------------------//
  
  CombineEfficiencies(KinEff_tot, std::vector<Efficiency>{Fiducial_eff, GeneratorEff, TriggerToSelEff});
  
  //check the total kinematic efficiency
  if(!CheckValidEfficiency(KinEff_tot)){
    log_errors_stream << "Warning: Kin_1 = " << Kin_1  << ", Kin_2 = " << Kin_2
                      << ", KinEff_tot.eff = " << KinEff_tot.efficiency
                      << " +- " << KinEff_tot.eff_err
                      << ", KinEff_tot.num_total = " << KinEff_tot.num_total
                      << ", KinEff_tot.num_passed = " << KinEff_tot.num_passed
                      << ". Setting unvalid efficiency." << std::endl;
    
    SetUnvalidEfficiency(KinEff_tot);
    
    return;  
  }
  
  return;
}

//-----------------------------------
// Get BDT efficiency for two cuts
//-----------------------------------
void effLUT::GetBDTEff(const double pt_1, const double fd_1,
                       const double pt_2, const double fd_2,
                       Efficiency &BDTEff_tot, const int& year){
  
  Efficiency BDTEff_1, BDTEff_2;
  
  //retrieve the single BDT components
  GetBDTComponents(pt_1, fd_1, pt_2, fd_2,
                   BDTEff_1, BDTEff_2, year);
  
  //check if the single efficiencies are fine
  if((!CheckValidEfficiency(BDTEff_1))
     || (!CheckValidEfficiency(BDTEff_2))){
    log_errors_stream << "Warning: BDTEff_1.eff = " << BDTEff_1.efficiency
                      << " +- " << BDTEff_1.eff_err
                      << ", BDTEff_2.eff = " << BDTEff_2.efficiency
                      << " +- " << BDTEff_2.eff_err
                      << ". Setting unvalid efficiency." << std::endl;
    
    SetUnvalidEfficiency(BDTEff_tot);
    
    return;
  }

  //--------------------------------//
  //  combine the BDT efficiencies  //
  //--------------------------------//
  
  CombineEfficiencies(BDTEff_tot, std::vector<Efficiency>{BDTEff_1, BDTEff_2});
  
  if(!CheckValidEfficiency(BDTEff_tot)){
    log_errors_stream << "Error: what the hell: the single efficiencies were fine, but the combined one is unvalid."
                      << ". BDTEff_tot.efficiency = " << BDTEff_tot.efficiency
                      << " +- " << BDTEff_tot.eff_err
                      << ". BDTEff_1.eff = " << BDTEff_1.efficiency
                      << " +- " << BDTEff_1.eff_err
                      << ", BDTEff_2.eff = " << BDTEff_2.efficiency
                      << " +- " << BDTEff_2.eff_err << std::endl;
    
    exit(EXIT_FAILURE);
  }
  
  return;
}

//-----------------------------------
// Get the single components of the BDT efficiencies
//-----------------------------------
void effLUT::GetBDTComponents(const double pt_1, const double fd_1,
                              const double pt_2, const double fd_2,
                              Efficiency &BDTEff_1, Efficiency &BDTEff_2, const int& year){
  
  //is the first BDT cut switched on?
  if(BDT1_eff_on){    
    if(adaptive_eff)
      h_BDT_eff_1_adaptive != nullptr ? GetSingleEff_adaptive(pt_1, fd_1, h_BDT_eff_1_adaptive, BDTEff_1) :
                                        GetSingleEff_adaptive_yr(pt_1, fd_1, year, BDT_eff_1_adaptive_plus_year, BDTEff_1);
    else
      GetSingleEff(pt_1, fd_1, h_BDT_eff_1, BDTEff_1);
  }
  else
    SetEfficiencyOne(BDTEff_1);
  
  //check the first BDT cut
  if(!CheckValidEfficiency(BDTEff_1)){
    
    log_errors_stream << "Warning: pt_1 = " << pt_1 << ", fd_1 = " << fd_1
                      << ", BDTEff_1.eff = " << BDTEff_1.efficiency
                      << " +- " << BDTEff_1.eff_err
                      << ", BDTEff_1.num_total = " << BDTEff_1.num_total
                      << ", BDTEff_1.num_passed = " << BDTEff_1.num_passed
                      << ". Setting unvalid efficiency." << std::endl;
    
    SetUnvalidEfficiency(BDTEff_1);
    
    return;
  }
  
  //is a second BDT cut enabled?
  if(BDT2_eff_on){
    if(adaptive_eff)
      h_BDT_eff_2_adaptive != nullptr ? GetSingleEff_adaptive(pt_2, fd_2, h_BDT_eff_2_adaptive, BDTEff_2) :
                                        GetSingleEff_adaptive_yr(pt_2, fd_2, year, BDT_eff_2_adaptive_plus_year, BDTEff_2);
    else
      GetSingleEff(pt_2, fd_2, h_BDT_eff_2, BDTEff_2);
  }
  else
    SetEfficiencyOne(BDTEff_2);
  
  //check the second BDT efficiency
  if(!CheckValidEfficiency(BDTEff_2)){
    
    log_errors_stream << "Warning: pt_2 = " << pt_2  << ", fd_2 = " << fd_2
                      << ", BDTEff_2.eff = " << BDTEff_2.efficiency
                      << " +- " << BDTEff_2.eff_err
                      << ", BDTEff_2.num_total = " << BDTEff_2.num_total
                      << ", BDTEff_2.num_passed = " << BDTEff_2.num_passed
                      << ". Setting unvalid efficiency." << std::endl;
    
    SetUnvalidEfficiency(BDTEff_2);
    
    return;
  }
  
  return;
}
		
//----------------------------- 
// Get efficiency for the combined PID cuts
//-----------------------------
void effLUT::GetPIDEff(const std::vector<TLorentzVector> PIDqvects,
                       const double nTracks, const bool varxP_varyEta,
                       Efficiency &PIDEff){
  
  //is the PID efficiency switched on?
  if(!PID_eff_on){
    SetEfficiencyOne(PIDEff);
    
    return;
  }
  
  //vector of efficiency structure, only temporarely
  std::vector<Efficiency> PIDEff_vect;
  
  //to handle efficiency maps with switched axis                                                                                                                                                  
  double var_x = -9999.;
  double var_y = -9999.;
  
  if(adaptive_eff){
    
    //first check that the number of PID particles and PID cuts are the same
    if(PIDqvects.size() != PID_eff_adaptive_vector->size()){
      std::cout << "Error: the number of PID particles and PID cuts is different." << std::endl;
      std::cout << "PIDqvects.size() = " << PIDqvects.size()
                << ", PID_eff_adaptive_vector->size() = " << PID_eff_adaptive_vector->size() << std::endl;
      exit(EXIT_FAILURE);
    }
    
    //find bin for these coordinates, but first check if the ranges are ok
    //test the x and y ranges on the first histo, it's fine
    //get<0>: zmin, get<1>: zmax, get<2>: TH2A histo
    
    unsigned int i_particle = 0;
    
    //loop over the PID cuts
    for(std::vector<std::vector<std::tuple<double,double,TH2A*>>>::const_iterator it_PIDcut = PID_eff_adaptive_vector->begin();
        it_PIDcut != PID_eff_adaptive_vector->end(); ++it_PIDcut, ++i_particle){
      
      //set the variables on the x and y axis
      if(varxP_varyEta){
        var_x = PIDqvects.at(i_particle).P();
        var_y = PIDqvects.at(i_particle).PseudoRapidity();
      }
      else{
        var_x = PIDqvects.at(i_particle).PseudoRapidity();
        var_y = PIDqvects.at(i_particle).P();
      }
      
      
      if( (var_x < std::get<2>((*it_PIDcut).front())->GetMinX()) || (std::get<2>((*it_PIDcut).front())->GetMaxX() < var_x)
          || (var_y < std::get<2>((*it_PIDcut).front())->GetMinY()) || (std::get<2>((*it_PIDcut).front())->GetMaxY() < var_y)
          || (nTracks < std::get<0>((*it_PIDcut).front())) || (std::get<1>((*it_PIDcut).back()) < nTracks)
          ){
        
        log_errors_stream << "Warning: PID kinematic values out of histo."
                          << " var_x = " << var_x
                          << " [" << std::get<2>((*it_PIDcut).front())->GetMinX()
                          << ", " << std::get<2>((*it_PIDcut).front())->GetMaxX() << "]"
                          << ", var_y = " << var_y
                          << " [" << std::get<2>((*it_PIDcut).front())->GetMinY()
                          << ", " << std::get<2>((*it_PIDcut).front())->GetMaxY() << "]" 
                          << ", ntracks = " << nTracks
                          << " [" << std::get<0>((*it_PIDcut).front())
                          << ", " << std::get<1>((*it_PIDcut).back()) << "]" 
                          << ". Setting dummy efficiency to -1, in order to be skipped." << std::endl;
        
        SetUnvalidEfficiency(PIDEff);
        
        return;
      }
    } //loop over the PID cuts

    //if I'm here, all the PID cuts are fine: let's continue
    
    i_particle = 0;
    
    //loop over the PID cuts
    for(std::vector<std::vector<std::tuple<double,double,TH2A*>>>::const_iterator it_PIDcut = PID_eff_adaptive_vector->begin();
        it_PIDcut != PID_eff_adaptive_vector->end(); ++it_PIDcut, ++i_particle){
      
      //search for the correct slice on the z axis: loop over vector elements
      for(std::vector<std::tuple<double,double,TH2A*>>::const_iterator it_slice = (*it_PIDcut).begin();
          it_slice != (*it_PIDcut).end(); ++it_slice){
        
        //remember:
        //get<0>: zmin, get<1>: zmax, get<2>: TH2A histo
        
        //check if nTracks is within the range of the current histo
        if((std::get<0>(*it_slice) > nTracks) || (nTracks > std::get<1>(*it_slice)))
          continue;
        
        int i_bin = (std::get<2>(*it_slice))->FindBin(var_x, var_y);
        
        //temp structure to store the data
        Efficiency PIDEff_temp;
        
        //efficiency
        PIDEff_temp.efficiency = (std::get<2>(*it_slice))->GetBinContent(i_bin);
        
        //errors
        PIDEff_temp.eff_err = (std::get<2>(*it_slice))->GetBinError(i_bin);
        PIDEff_temp.eff_err_low = (std::get<2>(*it_slice))->GetBinErrorLow(i_bin);
        PIDEff_temp.eff_err_up = (std::get<2>(*it_slice))->GetBinErrorUp(i_bin);
        
        //to understand how to correctly set them...
        PIDEff_temp.num_total = 1.;
        PIDEff_temp.num_passed = 1.;
      
        PIDEff_vect.push_back(PIDEff_temp);
        
      } //loop over slices
    }  //loop over cuts
  }
  else
  {
    
    //first check that the number of PID particles and PID cuts are the same
    if(PIDqvects.size() != h_PID_eff_vector.size()){
      std::cout << "Error: the number of PID particles and PID cuts is different." << std::endl;
      std::cout << "PIDqvects.size() = " << PIDqvects.size()
                << ", h_PID_eff_vector.size() = " << h_PID_eff_vector.size() << std::endl;
      exit(EXIT_FAILURE);
    }
    
    //find bin for these coordinates, but first check if the ranges are ok
    
    unsigned int i_particle = 0;
    
    //loop over the PID cuts
    for(std::vector<TEfficiency*>::const_iterator it_PIDcut = h_PID_eff_vector.begin();
        it_PIDcut != h_PID_eff_vector.end(); ++it_PIDcut, ++i_particle){
      
      //set the variables on the x and y axis                                                                                                                                                       
      if(varxP_varyEta){
        var_x = PIDqvects.at(i_particle).P();
        var_y = PIDqvects.at(i_particle).PseudoRapidity(); 
      }
      else{
        var_x = PIDqvects.at(i_particle).PseudoRapidity();
        var_y = PIDqvects.at(i_particle).P();
      }
      
      if( (var_x < (*it_PIDcut)->GetTotalHistogram()->GetXaxis()->GetXmin())
          || ((*it_PIDcut)->GetTotalHistogram()->GetXaxis()->GetXmax() < var_x)
          || (var_y < (*it_PIDcut)->GetTotalHistogram()->GetYaxis()->GetXmin())
          || ((*it_PIDcut)->GetTotalHistogram()->GetYaxis()->GetXmax() < var_y)
          || (nTracks < (*it_PIDcut)->GetTotalHistogram()->GetZaxis()->GetXmin())
          || ((*it_PIDcut)->GetTotalHistogram()->GetZaxis()->GetXmax() < nTracks)
          ){
        
        log_errors_stream << "Warning: PID kinematic values out of histo."
                          << " var_x = " << var_x
                          << " [" << (*it_PIDcut)->GetTotalHistogram()->GetXaxis()->GetXmin()
                          << ", " << (*it_PIDcut)->GetTotalHistogram()->GetXaxis()->GetXmax() << "]"
                          << ", var_y = " << var_y
                          << " [" << (*it_PIDcut)->GetTotalHistogram()->GetYaxis()->GetXmin()
                          << ", " << (*it_PIDcut)->GetTotalHistogram()->GetYaxis()->GetXmax() << "]"
                          << ", ntracks = " << nTracks
                          << " [" << (*it_PIDcut)->GetTotalHistogram()->GetZaxis()->GetXmin()
                          << ", " << (*it_PIDcut)->GetTotalHistogram()->GetZaxis()->GetXmax() << "]"
                          << ". Setting dummy efficiency to -1, in order to be skipped." << std::endl;
        
        SetUnvalidEfficiency(PIDEff);
        
        return;
      }
    }  //loop over cuts
    
    //if I'm here, all the PID cuts are fine: let's continue
    
    //vector of efficiency structure, only temporanely
    std::vector<Efficiency> PIDEff_vect;
    
    i_particle = 0;
    
    //loop over the PID cuts
    for(std::vector<TEfficiency*>::const_iterator it_PIDcut = h_PID_eff_vector.begin();
        it_PIDcut != h_PID_eff_vector.end(); ++it_PIDcut, ++i_particle){
      
      int i_bin = (*it_PIDcut)->FindFixBin(var_x, var_y, nTracks);
      
      //temp structure to store the data
      Efficiency PIDEff_temp;
      
      //efficiency
      PIDEff_temp.efficiency = (*it_PIDcut)->GetEfficiency(i_bin);
      
      //error
      PIDEff_temp.eff_err = ((*it_PIDcut)->GetEfficiencyErrorUp(i_bin) + (*it_PIDcut)->GetEfficiencyErrorLow(i_bin))/2.;
      PIDEff_temp.eff_err_low = (*it_PIDcut)->GetEfficiencyErrorLow(i_bin);
      PIDEff_temp.eff_err_up = (*it_PIDcut)->GetEfficiencyErrorUp(i_bin);
      
      //total and passed events
      PIDEff_temp.num_total = (*it_PIDcut)->GetTotalHistogram()->GetBinContent(i_bin);
      PIDEff_temp.num_passed = (*it_PIDcut)->GetPassedHistogram()->GetBinContent(i_bin);
 
      PIDEff_vect.push_back(PIDEff_temp);
      
    }  //loop over cuts  
  }  //if(adaptive_eff)
  
  //now combine all the single efficiencies
  CombineEfficiencies(PIDEff, PIDEff_vect);
  
  return;
}

//----------------------------------------------
// Get single components of the kinematic efficiency
//----------------------------------------------
bool effLUT::GetKinComponents(const double Kin_1, const double Kin_2,
                              Efficiency &GeneratorEff, Efficiency &FiducialEff,
                              Efficiency &TriggerToSelEff,
                              Efficiency &TriggerEff, Efficiency &RecoEff,
                              Efficiency &SelectionEff){
  
  //if it's switched off, return unvalid efficiencies
  if(!SingleKinComponents_eff_on){
    
    SetUnvalidEfficiency(GeneratorEff);
    SetUnvalidEfficiency(FiducialEff);
    SetUnvalidEfficiency(TriggerEff);
    SetUnvalidEfficiency(TriggerToSelEff);
    SetUnvalidEfficiency(RecoEff);
    SetUnvalidEfficiency(SelectionEff);
    
    return true;
  }
  
  //fiducial efficiency
  FiducialEff = Fiducial_eff;
   
  //generator efficiency
  if(Generator_eff_on){
    if(adaptive_eff)
      GetSingleEff_adaptive(Kin_1, Kin_2, h_generator_eff_adaptive, GeneratorEff);
    else
      GetSingleEff(Kin_1, Kin_2, h_generator_eff, GeneratorEff);
  }
  else
    SetEfficiencyOne(GeneratorEff);
  
  if(TriggerToSel_eff_on){
    
    if(adaptive_eff){
      //trigger efficiency
      GetSingleEff_adaptive(Kin_1, Kin_2, h_trigger_eff_adaptive, TriggerEff);
      
      //reconstruction efficiency
      GetSingleEff_adaptive(Kin_1, Kin_2, h_reco_eff_adaptive, RecoEff);
      
      //selection efficiency
      GetSingleEff_adaptive(Kin_1, Kin_2, h_selection_eff_adaptive, SelectionEff);
    
      //trigger-->selection efficiency
      GetSingleEff_adaptive(Kin_1, Kin_2, h_TriggerToSel_eff_adaptive, TriggerToSelEff);
    }
    else
    {
      //trigger efficiency
      GetSingleEff(Kin_1, Kin_2, h_trigger_eff, TriggerEff);
    
      //reconstruction efficiency
      GetSingleEff(Kin_1, Kin_2, h_reco_eff, RecoEff);
      
      //selection efficiency
      GetSingleEff(Kin_1, Kin_2, h_selection_eff, SelectionEff);
      
      //trigger-->selection efficiency
      GetSingleEff(Kin_1, Kin_2, h_TriggerToSel_eff, TriggerToSelEff);
    }  //if(adaptive_eff)
  }  //if(TriggerToSel_eff_on)
  else
  {
    SetEfficiencyOne(TriggerEff);
    SetEfficiencyOne(RecoEff);
    SetEfficiencyOne(SelectionEff);
    SetEfficiencyOne(TriggerToSelEff);
  }
  
  //now check the efficiencies
  if((!CheckValidEfficiency(GeneratorEff))
     || (!CheckValidEfficiency(TriggerEff))
     || (!CheckValidEfficiency(RecoEff))
     || (!CheckValidEfficiency(SelectionEff))
     || (!CheckValidEfficiency(TriggerToSelEff))
     )
    return false;
  else
    return true;
  
}


//------------
// Constructor
//------------
effLUT::effLUT(const std::string _configFileName, TFile* _outFile)
  : configFileName(_configFileName) {
  
  outFile = _outFile;
  
  GeneratorEffFile = NULL;
  h_generator_eff = NULL;
  h_generator_eff_adaptive = NULL;
  
  TriggerToSelEffFile = NULL;
  h_TriggerToSel_eff = NULL;
  h_TriggerToSel_eff_adaptive = NULL;
    
  bdtEffFile_1 = NULL;
  bdtEffFile_2 = NULL;
  
  h_BDT_eff_1 = NULL;
  h_BDT_eff_1_adaptive = NULL;
  h_BDT_eff_2 = NULL;
  h_BDT_eff_2_adaptive = NULL;
  
  //set the name of the error log stream, starting from the outFile name
  std::string log_name = outFile->GetName();
  
  boost::replace_all(log_name, ".root", "_errlog.log");
  
  std::cout << "log_name = " << log_name << std::endl;
  
  //open the output stream for the log
  log_errors_stream.open(log_name, ios::out);
  
  //write some informations in the out log
  log_errors_stream << "#log error file created by the effLUT library" << std::endl;
  log_errors_stream << "configFileName = " << _configFileName << std::endl;
  log_errors_stream << "outFileName = " << outFile->GetName() << std::endl;
  
  //-------------------------------//
  //  read the configuration file  //
  //-------------------------------//
  
  //parse the INFO into the property tree.
  pt::read_info(configFileName, configtree);
  
  //set which efficiencies are enabled
  Generator_eff_on = (bool) configtree.get<unsigned int>("kinEff.GeneratorEff");
  Fiducial_eff_on = (bool) configtree.get<unsigned int>("kinEff.FiducialEff");
  TriggerToSel_eff_on = (bool) configtree.get<unsigned int>("kinEff.TriggerToSelEff");
  BDT1_eff_on = (bool) configtree.get<unsigned int>("BDTEff.BDTEff_1");
  BDT2_eff_on = (bool) configtree.get<unsigned int>("BDTEff.BDTEff_2");
  PID_eff_on = (bool) configtree.get<unsigned int>("PIDEff.PIDEff");
  
  SingleKinComponents_eff_on = (bool) configtree.get<unsigned int>("kinEff.singleComp.status");

  if((bool) configtree.get<unsigned int>("UseAdaptive")){
    adaptive_eff = true;
    LoadAdaptive();
  }
  else
  {
    adaptive_eff = false;
    LoadTEfficiency();
  }
  
  return;
}

//---------------------------------------------------------
// To load the fiducial efficiency
//---------------------------------------------------------
void effLUT::LoadFiducialEff(){
  
  std::string input_filename = ParseEnvName(configtree.get<std::string>("kinEff.inFile_Fiducial"));
  
  std::cout << "Loading Fiducial efficiency from: " << input_filename << std::endl;
  
  //retrieve the fiducial efficiency from the .info file                                                                                                                                                       
  pt::ptree fiducial_file;
  pt::read_info(input_filename, fiducial_file);
  
  Fiducial_eff.efficiency = fiducial_file.get<double>("efficiency");
  
  Fiducial_eff.eff_err = fiducial_file.get<double>("error");
  Fiducial_eff.eff_err_low = fiducial_file.get<double>("error_low");
  Fiducial_eff.eff_err_up = fiducial_file.get<double>("error_up");
  
  Fiducial_eff.num_total = fiducial_file.get<double>("num_total");
  Fiducial_eff.num_passed = fiducial_file.get<double>("num_passed");
  
  return;
}

//---------------------------------------------------------
// To load TEfficiency objects
//---------------------------------------------------------
void effLUT::LoadTEfficiency(){
  
  std::string input_filename;

  //load the fiducial efficiency
  if(Fiducial_eff_on)
    LoadFiducialEff();
  else
    SetEfficiencyOne(Fiducial_eff);
  
  //-----------------------------------//
  //  load the kinematic efficiencies  //
  //-----------------------------------//
  
  //has the generator efficiency to be applied?
  if(Generator_eff_on){
    input_filename = ParseEnvName(configtree.get<std::string>("kinEff.inFile_Generator"));
    
    GeneratorEffFile = TFile::Open(input_filename.c_str(), "READ");
    
    std::cout << "Loading Generator efficiency histo from: " << input_filename << std::endl;
    
    if(GeneratorEffFile == NULL){
      std::cout<< "Error: generator level cut efficiency file " << input_filename << " does not exist." << std::endl;
      exit(EXIT_FAILURE);
    }
    
    //open histo with efficiency
    h_generator_eff = (TEfficiency*) GeneratorEffFile->Get((configtree.get<std::string>("kinEff.inHistoEff_Generator")).c_str());
    
    if(h_generator_eff == NULL){
      std::cout << "Error: Generator level cut efficiency histo does not exist." << std::endl;
      exit(EXIT_FAILURE);
    }
    
    //save the efficiency histo in the output file
    outFile->cd();
    h_generator_eff->Write(h_generator_eff->GetName());
    
  }  //if(Generator_eff_on)
  
  ////////
  
  //has the trigger->selection efficiency to be applied?
  if(TriggerToSel_eff_on){
    input_filename = ParseEnvName(configtree.get<std::string>("kinEff.inFile_TriggerToSel"));
    
    TriggerToSelEffFile = TFile::Open(input_filename.c_str(), "READ");
    
    std::cout << "Loading trigger->selection efficiency histo from: " << input_filename << std::endl;
    
    if(TriggerToSelEffFile == NULL){
      std::cout<< "Error: trigger->selection efficiency file " << input_filename << " does not exist." << std::endl;
      exit(EXIT_FAILURE);
    }
    
    //open histo with efficiency
    h_TriggerToSel_eff = (TEfficiency*) TriggerToSelEffFile->Get((configtree.get<std::string>("kinEff.inHistoEff_TriggerToSel")).c_str());
    
    if(h_TriggerToSel_eff == NULL){
      std::cout << "Error: h_TriggerToSel_eff efficiency histo does not exist." << std::endl;
      exit(EXIT_FAILURE);
    }
    
    //save the efficiency histo in the output file
    outFile->cd();
    h_TriggerToSel_eff->Write(h_TriggerToSel_eff->GetName());
    
  }  //if(TriggerToSel_eff_on)
  
  
  //-----------------------------//
  //  load the BDT efficiencies  //
  //-----------------------------//
  
  //is the first BDT cut to apply?
  if(BDT1_eff_on){
    
    input_filename = ParseEnvName(configtree.get<std::string>("BDTEff.inFile_1"));
    
    bdtEffFile_1 = TFile::Open(input_filename.c_str(), "READ");
    
    if(bdtEffFile_1 == NULL){
      std::cout << "Error: BDT_1 efficiency file " << input_filename << " does not exist." << std::endl;
      exit(EXIT_FAILURE);
    }
    
    std::cout << "Loading BDT_1 efficiency histo from: " << input_filename << std::endl;
    
    //open histo with data
    h_BDT_eff_1 = (TEfficiency*) bdtEffFile_1->Get((configtree.get<std::string>("BDTEff.inHistoEff_1")).c_str());
    
    if(h_BDT_eff_1 == NULL){
      std::cout << "Error: BDT_1 efficiency histo does not exist." << std::endl;
      exit(EXIT_FAILURE);
    }
    
    //save the efficiency histo in the output file
    outFile->cd();
    h_BDT_eff_1->Write(h_BDT_eff_1->GetName());
    
  }  //if((bool) configtree.get<std::string>("kinEff.BDTEff"))
  
  ////////
  
  //is the second BDT cut to apply?
  if(BDT2_eff_on){
    
    input_filename = ParseEnvName(configtree.get<std::string>("BDTEff.inFile_2"));
    
    bdtEffFile_2 = TFile::Open(input_filename.c_str(), "READ");
    
    if(bdtEffFile_2 == NULL){
      std::cout << "Error: BDT_2 efficiency file " << input_filename << " does not exist." << std::endl;
      exit(EXIT_FAILURE);
    }
    
    std::cout << "Loading BDT_2 efficiency histo from: " << input_filename << std::endl;
    
    //open histo with data
    h_BDT_eff_2 = (TEfficiency*) bdtEffFile_2->Get((configtree.get<std::string>("BDTEff.inHistoEff_2")).c_str());
    
    if(h_BDT_eff_2 == NULL){
      std::cout << "Error: BDT_2 efficiency histo does not exist." << std::endl;
      exit(EXIT_FAILURE);
    }
    
    //save the efficiency histo in the output file
    outFile->cd();
    h_BDT_eff_2->Write(h_BDT_eff_2->GetName());
    
  }  //if((bool) configtree.get<std::string>("kinEff.BDTEff"))
  
  
  //-----------------------------//
  //  load the PID efficiencies  //
  //-----------------------------//
  
  if(PID_eff_on){
    
    //loop over PID cuts
    for(pt::ptree::const_iterator PIDEff = configtree.get_child("PIDEff.PIDCuts").begin();
        PIDEff != configtree.get_child("PIDEff.PIDCuts").end(); ++PIDEff){
      
      input_filename = ParseEnvName(PIDEff->second.get<std::string>("inFile"));
      
      TFile *pidEffFile = TFile::Open(input_filename.c_str(), "READ");
      
      if(pidEffFile == NULL){
        std::cout << "Error: PID efficiency file " << input_filename << " does not exist." << std::endl;
        exit(EXIT_FAILURE);
      }
      
      std::cout << "Loading PID efficiency histo from: " << input_filename << std::endl;
      
      //open the efficiency histo, getting it from gDirectory in order to later detach the histo from the file 
      TEfficiency* h_PID_eff = (TEfficiency*) gDirectory->Get((PIDEff->second.get<std::string>("inHistoEff")).c_str());
      
      if(h_PID_eff == NULL){
        std::cout << "Error: PID efficiency histo does not exist." << std::endl;
        exit(EXIT_FAILURE);
      }
      
      //detach the histo from the input file
      h_PID_eff->SetDirectory(0);
      
      //save the efficiency histo in the output file
      outFile->cd();
      h_PID_eff->Write((h_PID_eff->GetName() + std::to_string(h_PID_eff_vector.size())).c_str());
      
      //save the current efficiency cut
      h_PID_eff_vector.push_back(h_PID_eff);
      
      //close the input file
      pidEffFile->Close();
      
    }  //loop over PID cuts
    
  }  //if((bool) configtree.get<std::string>("kinEff.PIDEff"))
  
  
  //---------------------------------------//
  //  set the single kinematic components  //
  //---------------------------------------//
  
  if(SingleKinComponents_eff_on){
    
    //trigger component
    input_filename = configtree.get<std::string>("kinEff.singleComp.trigger_file");
    
    TriggerEffFile = TFile::Open(input_filename.c_str(), "READ");
    
    if(TriggerEffFile == NULL){
      std::cout<< "TriggerEffFile not found." << std::endl;
      exit(EXIT_FAILURE);
    }
    
    std::cout << "Loading TriggerEffFile from: " << input_filename << std::endl;
    
    //open histo with efficiency
    h_trigger_eff = (TEfficiency*) TriggerEffFile->Get((configtree.get<std::string>("kinEff.singleComp.trigger_histo")).c_str());
    
    if(h_trigger_eff == NULL){
      std::cout << "h_trigger_eff not found" << std::endl;
      exit(EXIT_FAILURE);
    }
    
    //reconstruction + stripping component
    input_filename = configtree.get<std::string>("kinEff.singleComp.reco_file");
    
    RecoEffFile = TFile::Open(input_filename.c_str(), "READ");
    
    if(RecoEffFile == NULL){
      std::cout<< "RecoEffFile not found." << std::endl;
      exit(EXIT_FAILURE);
    }
    
    std::cout << "Loading RecoEffFile from: " << input_filename << std::endl;
    
    //open histo with efficiency
    h_reco_eff = (TEfficiency*) RecoEffFile->Get((configtree.get<std::string>("kinEff.singleComp.reco_histo")).c_str());
    
    if(h_reco_eff == NULL){
      std::cout << "h_reco_eff not found" << std::endl;
      exit(EXIT_FAILURE);
    }
    
    //selection
    input_filename = configtree.get<std::string>("kinEff.singleComp.selection_file");
    
    SelectionEffFile = TFile::Open(input_filename.c_str(), "READ");
    
    if(SelectionEffFile == NULL){
      std::cout<< "SelectionEffFile not found." << std::endl;
      exit(EXIT_FAILURE);
    }
    
    std::cout << "Loading SelectionEffFile from: " << input_filename << std::endl;
    
    //open histo with efficiency
    h_selection_eff = (TEfficiency*) SelectionEffFile->Get((configtree.get<std::string>("kinEff.singleComp.selection_histo")).c_str());
    
    if(h_selection_eff == NULL){
      std::cout << "h_selection_eff not found" << std::endl;
      exit(EXIT_FAILURE);
    }
    
  }  //if(SingleKinComponents_eff_on)
  
  return;
}


//---------------------------------------------------------
// To load TH2A objects
//---------------------------------------------------------
void effLUT::LoadAdaptive(){
  
  std::string input_filename;
  
  //load the fiducial efficiency
  if(Fiducial_eff_on)
    LoadFiducialEff();
  else
    SetEfficiencyOne(Fiducial_eff);
  
  std::cout << "Fiducial_eff.efficiency = " << Fiducial_eff.efficiency << std::endl;
  
  //-----------------------------------//
  //  load the kinematic efficiencies  //
  //-----------------------------------//
  
  //has the generator efficiency to be applied?
  if(Generator_eff_on){
    input_filename = ParseEnvName(configtree.get<std::string>("kinEff.inFile_Generator"));
    
    GeneratorEffFile = TFile::Open(input_filename.c_str(), "READ");
    
    std::cout << "Loading generator level cut efficiency histo from: " << input_filename << std::endl;
    
    if(GeneratorEffFile == NULL){
      std::cout<< "Error: generator level cut efficiency file " << input_filename << " does not exist." << std::endl;
      exit(EXIT_FAILURE);
    }
    
    //open histo with efficiency
    h_generator_eff_adaptive = (TH2A*) GeneratorEffFile->Get((configtree.get<std::string>("kinEff.inHistoEff_Generator")).c_str());
    
    if(h_generator_eff_adaptive == NULL){
      std::cout << "Error: Generator efficiency histo does not exist." << std::endl;
      exit(EXIT_FAILURE);
    }

    //save the efficiency histo in the output file
    outFile->cd();
    h_generator_eff_adaptive->Write(h_generator_eff_adaptive->GetName());
    
  }  //if(Generator_eff_on)
  
  ////////
  
  //has the trigger->selection efficiency to be applied?
  if(TriggerToSel_eff_on){
    input_filename = ParseEnvName(configtree.get<std::string>("kinEff.inFile_TriggerToSel"));
    
    TriggerToSelEffFile = TFile::Open(input_filename.c_str(), "READ");
    
    std::cout << "Loading trigger->selection efficiency histo from: " << input_filename << std::endl;
    
    if(TriggerToSelEffFile == NULL){
      std::cout<< "Error: trigger->seleciton efficiency file " << input_filename << " does not exist." << std::endl;
      exit(EXIT_FAILURE);
    }
    
    //open histo with efficiency
    h_TriggerToSel_eff_adaptive = (TH2A*) TriggerToSelEffFile->Get((configtree.get<std::string>("kinEff.inHistoEff_TriggerToSel")).c_str());
    
    if(h_TriggerToSel_eff_adaptive == NULL){
      std::cout << "Error: h_TriggerToSel_eff_adaptive efficiency histo does not exist." << std::endl;
      exit(EXIT_FAILURE);
    }
    
    //save the efficiency histo in the output file
    outFile->cd();
    h_TriggerToSel_eff_adaptive->Write(h_TriggerToSel_eff_adaptive->GetName());
    
  }  //if(TriggerToSel_eff_on)
  
  
  //-----------------------------//
  //  load the BDT efficiencies  //
  //-----------------------------//
  
  //is the first BDT cut to apply?
  if(BDT1_eff_on){
    
    input_filename = ParseEnvName(configtree.get<std::string>("BDTEff.inFile_1"));

    if(input_filename.find("#") == std::string::npos){

      std::cout << "Loading BDT_1 efficiency histo from: " << input_filename << std::endl;

      bdtEffFile_1 = TFile::Open(input_filename.c_str(), "READ");

      if(bdtEffFile_1 == NULL){
        std::cout << "Error: BDT_1 efficiency file " << input_filename << " does not exist." << std::endl;
        exit(EXIT_FAILURE);
      }

      //open histo with data
      const auto optionalLUTinputhistname = configtree.get_optional<std::string>("BDTEff.inHistoEff_1");
      if(optionalLUTinputhistname)
        h_BDT_eff_1_adaptive = (TH2A*) bdtEffFile_1->Get((*optionalLUTinputhistname).c_str());
      else
        //dirty little trick: get the first object in the file and cast it to TH2A (there should only be the map in the file)
        h_BDT_eff_1_adaptive = static_cast<TH2A*>(static_cast<TKey*>(gDirectory->GetListOfKeys()->First())->ReadObj());


      if(h_BDT_eff_1_adaptive == NULL){
        std::cout << "Error: BDT_1 efficiency histo does not exist." << std::endl;
        exit(EXIT_FAILURE);
      }

      //save the efficiency histo in the output file
      outFile->cd();
      h_BDT_eff_1_adaptive->Write(h_BDT_eff_1_adaptive->GetName());
    }
    else{
      TString copied_input_filename_for_stupid_string_manipulation_since_STL_is_plain_shit_when_it_comes_to_this = input_filename;
      auto obja = copied_input_filename_for_stupid_string_manipulation_since_STL_is_plain_shit_when_it_comes_to_this.Tokenize("#");
      for(typename std::decay<decltype(obja->GetEntries())>::type dsdbsdfb = 0; dsdbsdfb < obja->GetEntries(); dsdbsdfb++){
        const auto filenamewithyearinit = static_cast<TObjString*>(obja->At(dsdbsdfb))->String();
        bdtEffFile_1 = TFile::Open(filenamewithyearinit.Data(),"READ");
        if(bdtEffFile_1 == NULL){
          std::cout << "Error: BDT_1 efficiency file " << filenamewithyearinit.Data() << " does not exist." << std::endl;
          exit(EXIT_FAILURE);
        }
        std::cout << "Loading BDT_1 efficiency histo from: " << filenamewithyearinit.Data() << std::endl;

        //distill year from filename
        int year = 0;
        auto anotherobja = filenamewithyearinit.Tokenize("_");
        for(typename std::decay<decltype(anotherobja->GetEntries())>::type sdv = 0; sdv < anotherobja->GetEntries(); sdv++)
          if(static_cast<TObjString*>(anotherobja->At(sdv))->String().IsDec())
            year = static_cast<TObjString*>(anotherobja->At(sdv))->String().Atoi();

        //open histo with data
        const auto optionalLUTinputhistname = configtree.get_optional<std::string>("BDTEff.inHistoEff_1");
        if(optionalLUTinputhistname)
          BDT_eff_1_adaptive_plus_year.emplace_back((TH2A*) bdtEffFile_1->Get((*optionalLUTinputhistname).c_str()),year);
        else
          //dirty little trick: get the first object in the file and cast it to TH2A (there should only be the map in the file)
          BDT_eff_1_adaptive_plus_year.emplace_back(static_cast<TH2A*>(static_cast<TKey*>(gDirectory->GetListOfKeys()->First())->ReadObj()),year);

        if(BDT_eff_1_adaptive_plus_year.back().first == nullptr){
          std::cout << "Error: BDT_1 efficiency histo does not exist." << std::endl;
          exit(EXIT_FAILURE);
        }

        //save the efficiency histo in the output file
        outFile->cd();
        BDT_eff_1_adaptive_plus_year.back().first->Write(BDT_eff_1_adaptive_plus_year.back().first->GetName());

      }
    }
  }  //if((bool) configtree.get<std::string>("kinEff.BDTEff"))
  
  ////////
  
  //is the second BDT cut to apply?
  if(BDT2_eff_on){
    
    input_filename = ParseEnvName(configtree.get<std::string>("BDTEff.inFile_2"));

    if(input_filename.find(std::string("#")) == std::string::npos){
      bdtEffFile_2 = TFile::Open(input_filename.c_str(), "READ");

      if(bdtEffFile_2 == NULL){
        std::cout << "Error: BDT_2 efficiency file " << input_filename << " does not exist." << std::endl;
        exit(EXIT_FAILURE);
      }

      std::cout << "Loading BDT_2 efficiency histo from: " << input_filename << std::endl;

      //open histo with data
      const auto optionalLUTinputhistname = configtree.get_optional<std::string>("BDTEff.inHistoEff_2");
      if(optionalLUTinputhistname)
        h_BDT_eff_2_adaptive = (TH2A*) bdtEffFile_2->Get((*optionalLUTinputhistname).c_str());
      else
        h_BDT_eff_2_adaptive = static_cast<TH2A*>(static_cast<TKey*>(gDirectory->GetListOfKeys()->First())->ReadObj());


      if(h_BDT_eff_2_adaptive == NULL){
        std::cout << "Error: BDT_2 efficiency histo does not exist." << std::endl;
        exit(EXIT_FAILURE);
      }

      //save the efficiency histo in the output file
      outFile->cd();
      h_BDT_eff_2_adaptive->Write(h_BDT_eff_2_adaptive->GetName());
    }
    else{
      TString copied_input_filename_for_stupid_string_manipulation_since_STL_is_plain_shit_when_it_comes_to_this = input_filename;
      auto obja = copied_input_filename_for_stupid_string_manipulation_since_STL_is_plain_shit_when_it_comes_to_this.Tokenize("#");
      for(typename std::decay<decltype(obja->GetEntries())>::type dsdbsdfb = 0; dsdbsdfb < obja->GetEntries(); dsdbsdfb++){
        const auto filenamewithyearinit = static_cast<TObjString*>(obja->At(dsdbsdfb))->String();
        bdtEffFile_2 = TFile::Open(filenamewithyearinit.Data(),"READ");
        if(bdtEffFile_2 == NULL){
          std::cout << "Error: BDT_2 efficiency file " << filenamewithyearinit.Data() << " does not exist." << std::endl;
          exit(EXIT_FAILURE);
        }
        std::cout << "Loading BDT_2 efficiency histo from: " << filenamewithyearinit.Data() << std::endl;

        //distill year from filename
        int year = 0;
        const auto anotherobja = filenamewithyearinit.Tokenize("_");
        for(typename std::decay<decltype(anotherobja->GetEntries())>::type sdv = 0; sdv < anotherobja->GetEntries(); sdv++)
          if(static_cast<TObjString*>(anotherobja->At(sdv))->String().IsDec())
            year = static_cast<TObjString*>(anotherobja->At(sdv))->String().Atoi();

        //open histo with data
        const auto optionalLUTinputhistname = configtree.get_optional<std::string>("BDTEff.inHistoEff_2");
        if(optionalLUTinputhistname)
          BDT_eff_2_adaptive_plus_year.emplace_back((TH2A*) bdtEffFile_2->Get((*optionalLUTinputhistname).c_str()),year);
        else
          BDT_eff_2_adaptive_plus_year.emplace_back(static_cast<TH2A*>(static_cast<TKey*>(gDirectory->GetListOfKeys()->First())->ReadObj()),year);

        if(BDT_eff_2_adaptive_plus_year.back().first == nullptr){
          std::cout << "Error: BDT_2 efficiency histo does not exist." << std::endl;
          exit(EXIT_FAILURE);
        }

        //save the efficiency histo in the output file
        outFile->cd();
        BDT_eff_2_adaptive_plus_year.back().first->Write(BDT_eff_2_adaptive_plus_year.back().first->GetName());

      }
    }
  }  //if((bool) configtree.get<std::string>("kinEff.BDTEff"))
  
  
  //-----------------------------//
  //  load the PID efficiencies  //
  //-----------------------------//
  
  if(PID_eff_on){
    
    PID_eff_adaptive_vector = new std::vector<std::vector<std::tuple<double,double,TH2A*>>>();
    
    //loop over PID cuts
    for(pt::ptree::const_iterator PIDEff = configtree.get_child("PIDEff.PIDCuts").begin();
        PIDEff != configtree.get_child("PIDEff.PIDCuts").end(); ++PIDEff){
      
      input_filename = ParseEnvName(PIDEff->second.get<std::string>("inFile"));
      
      TFile *pidEffFile = TFile::Open(input_filename.c_str(), "READ");
      
      if(pidEffFile == NULL){
        std::cout << "Error: PID efficiency file " << input_filename << " does not exist." << std::endl;
        exit(EXIT_FAILURE);
      }
      
      std::cout << "Loading PID efficiency histo from: " << input_filename << std::endl;
      
      //open the vector of efficiency histos
      //PID_eff_adaptive_vector = (std::vector<std::tuple<double,double,TH2A*>>*) pidEffFile->Get((configtree.get<std::string>("PIDEff.inHistoEff")).c_str());
      
      //if(PID_eff_adaptive_vector == NULL){
      //  std::cout << "Error: the vector of PID efficiency histos does not exist." << std::endl;
      //  exit(EXIT_FAILURE);
      //}
      
      //save the efficiency histos in the output file
      //outFile->cd();
      //gFile->WriteObject(&PID_eff_adaptive_vector, "PID_eff_adaptive_vector");
      
      //load the different adaptive histos, and store them in a tuple
      TVectorD *zBins_vect = (TVectorD *) pidEffFile->Get((PIDEff->second.get<std::string>("inHistoEff") + "_numZbins").c_str());
      
      if(zBins_vect == NULL){
        std::cout << "Error: the vector with the number of zBins for the PID adaptive does not exist." << std::endl;
        exit(EXIT_FAILURE);
      }
      
      int numZbins = (*zBins_vect)[0];
      
      std::vector<std::tuple<double,double,TH2A*>> PID_eff_adaptive_vector_temp;
      
      //loop over input histos
      for(int i_Zbin = 1; i_Zbin <= numZbins; i_Zbin++){
        
        std::string histo_name = PIDEff->second.get<std::string>("inHistoEff") + "_" + std::to_string(i_Zbin);
        
        TH2A *PID_eff_adaptive_curr = (TH2A*) ((TH2A*) pidEffFile->Get(histo_name.c_str()))->Clone();
        
        if(PID_eff_adaptive_curr == NULL){
          std::cout << "Error: the PID adaptive histo " << histo_name << std::endl;
          exit(EXIT_FAILURE);
        }
        
        //now I set the boundaries of the nTracks slices
        std::string zRange_name = PIDEff->second.get<std::string>("inHistoEff") + "_" +std::to_string(i_Zbin) +"_zRanges";
        
        TVectorD *zRanges = (TVectorD *) pidEffFile->Get(zRange_name.c_str());
        
        if(zRanges == NULL){
          std::cout << "Error: the vector with zRanges " << zRange_name << " for the PID adaptive does not exist." << std::endl;
          exit(EXIT_FAILURE);  
        }
        
        PID_eff_adaptive_vector_temp.push_back(std::make_tuple((*zRanges)[0], (*zRanges)[1],
                                                               PID_eff_adaptive_curr));
        
      }  //loop over input histos
      
      //a check of the loaded vector
      for(std::vector<std::tuple<double,double,TH2A*>>::const_iterator it_vect = PID_eff_adaptive_vector_temp.begin();
          it_vect != PID_eff_adaptive_vector_temp.end(); ++it_vect)
        std::cout << "nTracks min = " << std::get<0>(*it_vect) << ", max = " << std::get<1>(*it_vect) << std::endl;
      
      //add the current PID cut
      PID_eff_adaptive_vector->push_back(PID_eff_adaptive_vector_temp);
      
      //close the input file
      pidEffFile->Close();
      
    }  //loop over the PID cuts
  }  //if((bool) configtree.get<std::string>("kinEff.PIDEff"))
  
  
  //---------------------------------------//
  //  set the single kinematic components  //
  //---------------------------------------//
  
  if(SingleKinComponents_eff_on){
    
    //trigger component
    input_filename = ParseEnvName(configtree.get<std::string>("kinEff.singleComp.trigger_file"));
    
    TriggerEffFile = TFile::Open(input_filename.c_str(), "READ");
    
    if(TriggerEffFile == NULL){
      std::cout<< "Error: TriggerEffFile " << input_filename << " does not exist." << std::endl;
      exit(EXIT_FAILURE);
    }
    
    std::cout << "Loading TriggerEffFile from: " << input_filename << std::endl;
    
    //open histo with efficiency
    h_trigger_eff_adaptive = (TH2A*) TriggerEffFile->Get((configtree.get<std::string>("kinEff.singleComp.trigger_histo")).c_str());
    
    if(h_trigger_eff_adaptive == NULL){
      std::cout << "h_trigger_eff not found" << std::endl;
      exit(EXIT_FAILURE);
    }
    
    //reconstruction + stripping component
    input_filename = ParseEnvName(configtree.get<std::string>("kinEff.singleComp.reco_file"));
    
    RecoEffFile = TFile::Open(input_filename.c_str(), "READ");
    
    if(RecoEffFile == NULL){
      std::cout<< "Error: RecoEffFile " << input_filename << " does not exist." << std::endl;
      exit(EXIT_FAILURE);
    }
    
    std::cout << "Loading RecoEffFile from: " << input_filename << std::endl;
    
    //open histo with efficiency
    h_reco_eff_adaptive = (TH2A*) RecoEffFile->Get((configtree.get<std::string>("kinEff.singleComp.reco_histo")).c_str());
    
    if(h_reco_eff_adaptive == NULL){
      std::cout << "h_reco_eff not found" << std::endl;
      exit(EXIT_FAILURE);
    }
    
    //selection
    input_filename = ParseEnvName(configtree.get<std::string>("kinEff.singleComp.selection_file"));
    
    SelectionEffFile = TFile::Open(input_filename.c_str(), "READ");
    
    if(SelectionEffFile == NULL){
      std::cout<< "SelectionEffFile " << input_filename << " does not exist." << std::endl;
      exit(EXIT_FAILURE);
    }
    
    std::cout << "Loading SelectionEffFile from: " << input_filename << std::endl;
    
    //open histo with efficiency
    h_selection_eff_adaptive = (TH2A*) SelectionEffFile->Get((configtree.get<std::string>("kinEff.singleComp.selection_histo")).c_str());
    
    if(h_selection_eff_adaptive == NULL){
      std::cout << "h_selection_eff not found" << std::endl;
      exit(EXIT_FAILURE);
    }
    
  }  //if(SingleKinComponents_eff_on)
  
  return;
  
}


//-----------
// Destructor
//-----------
effLUT::~effLUT(){
  if(GeneratorEffFile != NULL)
    GeneratorEffFile->Close();
  
  if(TriggerToSelEffFile != NULL)
    TriggerToSelEffFile->Close();
  
  if(bdtEffFile_1 != NULL)
    bdtEffFile_1->Close();
  
  if(bdtEffFile_2 != NULL)
    bdtEffFile_2->Close();
}
