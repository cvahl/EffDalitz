#ifndef INCLUDE_EFFLUT_H 
#define INCLUDE_EFFLUT_H 1

/** @class effLUT effLUT.h include/effLUT.h               
 *                                                  
 *                                                                   
 *  @author   Alessio Piucci                                              
 *  @date     2016-03-15                                             
 *  @brief    Script to retrieve the efficiency value for a given candidate, reading from lookup tables.             
 *  @return   Returns the efficiency value.                                                               
 */

// Include files
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string>
#include <utility>   //to use std::pair
#include <tuple>
#include <fstream>   //to write the log in an external stream

#include "commonLib.h"
#include "../2DAdaptiveBinning/include/TH2A.h"

//ROOT libraries
#include "TString.h"
#include "TH2D.h"
#include "TLorentzVector.h"
#include "TFile.h"
#include "TH2D.h"
#include "TH3D.h"
#include "TEfficiency.h"
#include "TVectorD.h"
#include "TKey.h"
#include "TObjString.h"

//Boost libraries
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string/replace.hpp>

using namespace std;
namespace pt = boost::property_tree;

struct Efficiency{
  
  //efficiency value
  double efficiency;
  
  //efficiency errors
  double eff_err;
  double eff_err_low;
  double eff_err_up;
  
  //total and passed number of events
  double num_total;
  double num_passed;
};


class effLUT {

public: 
  
  /// Standard constructor
  effLUT(const std::string _configFileName, TFile* _outFile); 

  ~effLUT( ); ///< Destructor
  
  // get efficiency as function of full kinematics of the event
  void GetEventEff(const double Kin_1, const double Kin_2,
                   const double BDTpt_1, const double BDTfd_1,
                   const double BDTpt_2, const double BDTfd_2,
                   const std::vector<TLorentzVector> PIDqvects,
                   const unsigned int nTracks, const bool varxP_varyEta,
                   Efficiency &KinEff_tot, Efficiency &BDTEff_tot, Efficiency &PIDEff_tot,
                   Efficiency &comb_eff, const int &year = 0);
  
  // get efficiency of kinematic selection from a Dalitz plot
  // with part1-part2 and part2-part3 invariant masses
  void GetKineEff(const double Kin_1, const double Kin_2,
                  Efficiency &KinEff_tot);
  
  // get the BDT efficiency for two cuts
  void GetBDTEff(const double pt_1, const double fd_1,
                 const double pt_2, const double fd_2,
                 Efficiency &BDTEff_tot, const int &year = 0);
  
  // get the efficiency for the combined PID cuts
  void GetPIDEff(const std::vector<TLorentzVector> PIDqvects,
                 const double nTracks, const bool varxP_varyEta,
                 Efficiency &PIDEff);
  
  // get a single efficiency from a 2D TEfficiency object, with total of total and passed events
  void GetSingleEff(const double var_1, const double var_2,
                    TEfficiency *h_eff, Efficiency &Eff);
  
  // get a single efficiency from a TH2A object, with total of total and passed events
  void GetSingleEff_adaptive(const double var_1, const double var_2,
                             TH2A *h_eff, Efficiency &Eff);

  void GetSingleEff_adaptive_yr(const double& var_1, const double& var_2, const int& year,
                                std::vector<std::pair<TH2A*,int>>& hs_eff, Efficiency &Eff);
  
  // get the single components of the kinematic efficiency
  bool GetKinComponents(const double Kin_1, const double Kin_2,
                        Efficiency &GeneratorEff, Efficiency &FiducialEff,
                        Efficiency &TriggerToSelEff,
                        Efficiency &TriggerEff, Efficiency &RecoEff,
                        Efficiency &SelectionEff);
  
  // get the single components of the BDT efficiencies
  void GetBDTComponents(const double pt_1, const double fd_1,
                        const double pt_2, const double fd_2,
                        Efficiency &BDTEff_1, Efficiency &BDTEff_2, const int &year = 0);
    
  // to load the fiducial efficiency
  void LoadFiducialEff();
  
  // to load TEfficiency objects
  void LoadTEfficiency();
  
  // to load TH2A objects
  void LoadAdaptive();
  
  // pair of particle names and 4-momenta for BDT and PID cuts
  std::string GetParticleNameBDT(){return configtree.get<std::string>("BDTEff.name_part");};
  std::string GetParticleNamePID(){return configtree.get<std::string>("PIDEff.name_part");};

  // to combine single efficiencies
  void CombineEfficiencies(Efficiency &TotEff, const std::vector<Efficiency> Eff_vect);
  
  // set dummy values for the efficiency
  void SetUnvalidEfficiency(Efficiency &Eff){
    Eff.efficiency = -1.;
    Eff.eff_err = -1.;
    Eff.eff_err_low = -1.;
    Eff.eff_err_up = -1.;
    Eff.num_total = -1.;
    Eff.num_passed = -1.;
  };

  // set dummy values for the efficiency, = 1.
  void SetEfficiencyOne(Efficiency &Eff){
    Eff.efficiency = 1.;
    Eff.eff_err = 0.;
    Eff.eff_err_low = 0.;
    Eff.eff_err_up = 0.;
    Eff.num_total = 1.;
    Eff.num_passed = 1.; 
  };
  
  // set dummy values for the efficiency, = 0.
  void SetNullEfficiency(Efficiency &Eff){
    Eff.efficiency = 0.;
    Eff.eff_err = 0.;
    Eff.eff_err_low = 0.;
    Eff.eff_err_up = 0.;
    Eff.num_total = 0.;
    Eff.num_passed = 0.; 
  };
  
  // to check if an efficiency is valid
  bool CheckValidEfficiency(const Efficiency Eff){
    return !(std::isnan(Eff.efficiency) || (Eff.efficiency <= 0.) || (Eff.efficiency > 1.000001)
             || std::isnan(Eff.eff_err) || (Eff.eff_err < 0.) || (Eff.eff_err > 1.000001));
  };
  
protected:
  
private:

  //to flag which efficiencies are enabled
  bool Generator_eff_on;
  bool Fiducial_eff_on;
  bool TriggerToSel_eff_on;
  bool BDT1_eff_on;
  bool BDT2_eff_on;
  bool PID_eff_on;

  bool SingleKinComponents_eff_on;
  
  //fiducial efficiency
  Efficiency Fiducial_eff;
  
  //variables for the kinematic efficiency, for two cuts
  TFile* GeneratorEffFile;        // file containing the kinematic efficiencies
  TEfficiency* h_generator_eff;   // kinematic efficiency
  TH2A* h_generator_eff_adaptive;
  
  TFile* TriggerToSelEffFile;        // file containing the kinematic efficiencies
  TEfficiency* h_TriggerToSel_eff;   // kinematic efficiency
  TH2A* h_TriggerToSel_eff_adaptive;
  
  //variables for the single components of the kinematic efficiency (trigger->selection)  
  TFile* TriggerEffFile;
  TEfficiency* h_trigger_eff;
  TH2A* h_trigger_eff_adaptive;
  
  TFile* RecoEffFile;
  TEfficiency* h_reco_eff;
  TH2A* h_reco_eff_adaptive;
  
  TFile* SelectionEffFile;
  TEfficiency* h_selection_eff;
  TH2A* h_selection_eff_adaptive;
  
  //variables for the BDT efficiency, for two cuts
  double bdtcut_1;
  TFile* bdtEffFile_1;     // file containing the bdt efficiencies
  TEfficiency *h_BDT_eff_1;  //histo for efficiency
  TH2A *h_BDT_eff_1_adaptive;
  std::vector<std::pair<TH2A*,int>> BDT_eff_1_adaptive_plus_year;

  double bdtcut_2;
  TFile* bdtEffFile_2;
  TEfficiency *h_BDT_eff_2;  //histo for efficiency  
  TH2A *h_BDT_eff_2_adaptive;
  std::vector<std::pair<TH2A*,int>> BDT_eff_2_adaptive_plus_year;

  //variables for the PID cuts
  std::vector<TEfficiency*> h_PID_eff_vector;  //vector of cut efficiencies
  std::vector<std::vector<std::tuple<double,double,TH2A*>>> *PID_eff_adaptive_vector;
  
  //config file
  std::string configFileName;
  pt::ptree configtree;

  //output file with the used TEfficiency objects
  TFile* outFile;

  //use adaptive binning?
  bool adaptive_eff;
  
  //variables used for combining efficiency
  double alpha;
  double beta;
  double conf_level;
  
  //error log
  std::ofstream log_errors_stream;
  
};

#endif // INCLUDE_EFFLUT_H
